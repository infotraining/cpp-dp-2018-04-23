#include "singleton.hpp"
#include <iostream>

using namespace std;

struct Logger
{
	void log()
	{
		cout << "Logger is logging" << endl;
	}
};

int main()
{
    Singleton::instance().do_something();

    Singleton& singleObject = Singleton::instance();
    singleObject.do_something();

	VS_Less_2015::SingletonHolder<Logger>::instance().log();
}
