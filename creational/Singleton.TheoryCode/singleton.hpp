#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

#include <iostream>
#include <mutex>
#include <atomic>

class Singleton
{
public:
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

    static Singleton& instance()
    {
		static Singleton unique_instance;
	
        return unique_instance;
    }

    void do_something();

private:

    Singleton() // disallows creation of new instances outside the class
    {
        std::cout << "Constructor of singleton" << std::endl;
    }

    ~Singleton()
    {
        std::cout << "Singleton has been destroyed!" << std::endl;
    }
};

void Singleton::do_something()
{
    std::cout << "Singleton instance at " << std::hex << &instance() << std::endl;
}


namespace VS_Less_2015
{
	template <typename T>
	class SingletonHolder
	{
	private:
		SingletonHolder() = default;
		~SingletonHolder() = default;

		static std::unique_ptr<T> instance_;
		static std::once_flag init_flag_;
	public:
		SingletonHolder(const SingletonHolder&) = delete;
		SingletonHolder& operator=(const SingletonHolder&) = delete;

		static T& instance()
		{
			std::call_once(init_flag_, [] { instance_ = std::make_unique<T>(); });

			return *instance_;
		}
	};

	template <typename T>
	std::once_flag SingletonHolder<T>::init_flag_;

	template <typename T>
	std::unique_ptr<T> SingletonHolder<T>::instance_;
}

#endif /*SINGLETON_HPP_*/
