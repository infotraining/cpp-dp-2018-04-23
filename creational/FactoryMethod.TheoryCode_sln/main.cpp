#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "factory.hpp"
#include <functional>
#include <set>

using namespace std;

namespace Alt
{
	class Service
	{
		shared_ptr<Logger> logger_;
	public:
		explicit Service(shared_ptr<Logger> logger) : logger_{logger}
		{			
		}

		void use()
		{
			logger_->log("1");
			//run();
			logger_->log("2");
		}
	};

	class ServiceTestable
	{		
	public:
		explicit Service()
		{
		}

		virtual ~ServiceTestable() = default;

		void use()
		{
			get_logger().log("1");
			//run();
			get_logger().log("2");
		}
	protected:
		virtual Logger& get_logger()
		{
			return Logger::instance();
		}
	};
}

namespace Canonical
{

	class Service
	{
		shared_ptr<LoggerCreator> creator_;

	public:
		Service(shared_ptr<LoggerCreator> creator) : creator_(creator)
		{
		}

		Service(const Service&) = delete;
		Service& operator=(const Service&) = delete;

		void use()
		{
			unique_ptr<Logger> logger = creator_->create_logger();
			logger->log("Client::use() has been started...");
			run();
			logger->log("Client::use() has finished...");
		}
	protected:
		virtual void run() {}
	};

}

namespace ModernCpp	
{
	using LoggerCreator = function<std::unique_ptr<Logger>()>;

	class Service
	{
		LoggerCreator log_factory_;

	public:
		Service(LoggerCreator creator) : log_factory_(creator)
		{
		}

		Service(const Service&) = delete;
		Service& operator=(const Service&) = delete;

		void use()
		{
			unique_ptr<Logger> logger = log_factory_();
			logger->log("Client::use() has been started...");
			run();
			logger->log("Client::use() has finished...");
		}
	protected:
		virtual void run() {}
	};
}

using LoggerFactory = std::unordered_map<std::string, ModernCpp::LoggerCreator>;

int main()
{
    LoggerFactory logger_factory;
	logger_factory.insert(make_pair("ConsoleLogger", &make_unique<ConsoleLogger>));
	logger_factory.insert(make_pair("FileLogger", [] { return make_unique<FileLogger>("log.dat"); }));
	logger_factory.insert(make_pair("DbLogger", [] { return make_unique<DbLogger>("sql server"); }));

    ModernCpp::Service srv(logger_factory.at("DbLogger"));
    srv.use();
}

void iterator_and_factory_method()
{	
	set<int> vec = { 1, 2, 3, 4 , 6 };

	for(const auto& item : vec)
	{
		cout << item << endl;
	}

	for(auto it = vec.begin(); it != vec.end(); ++it)
	{
		const auto& item = *it;
		cout << item << endl;
	}
}
