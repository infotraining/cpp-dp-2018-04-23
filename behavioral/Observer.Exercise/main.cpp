#include "stock.hpp"

using namespace std;

int main()
{
    Stock misys("Misys", 340.0);
    Stock ibm("IBM", 245.0);
    Stock tpsa("TPSA", 95.0);

	auto solorz = make_shared<Investor>("Solorz");
	{
		auto kulczyk_jr = make_shared<Investor>("Kulczyk Jr");

		ibm.attach(solorz);
		ibm.attach(kulczyk_jr);
		tpsa.attach(solorz);
		misys.attach(kulczyk_jr);

		// zmian kursow
		misys.set_price(360.0);
		ibm.set_price(210.0);
		tpsa.set_price(45.0);
	}
	
	cout << "\n\n";

    misys.set_price(380.0);
    ibm.set_price(230.0);
    tpsa.set_price(15.0);
}
