#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <iostream>
#include <string>
#include <set>
#include <memory>

struct PriceChangedEventArgs
{
	std::string symbol;
	double price;
};

class Observer
{
public:
    virtual void update(const PriceChangedEventArgs& args) = 0;
    virtual ~Observer()
    {
    }
};

// Subject
class Stock
{
private:
    std::string symbol_;
    double price_;
	std::set<std::weak_ptr<Observer>, std::owner_less<std::weak_ptr<Observer>>> observers_;
public:
    Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
    {
    }

    std::string get_symbol() const
    {
        return symbol_;
    }

    double get_price() const
    {
        return price_;
    }

    // TODO: rejestracja obserwatora
	void attach(std::shared_ptr<Observer> observer)
    {
		observers_.insert(observer);
    }

    // TODO: wyrejestrowanie obserwatora
	void detach(std::shared_ptr<Observer> observer)
    {
		observers_.erase(observer);
    }

    void set_price(double price)
    {
		if (price != price_)
		{
			price_ = price;
			notify();
		}        
    }
protected:
	void notify()
	{
		auto it = observers_.begin();
		while (it != observers_.end())
		{
			auto living_observer = it->lock();
			if (living_observer)
			{
				living_observer->update(PriceChangedEventArgs{ symbol_, price_ });
				++it;
			}
			else
			{
				it = observers_.erase(it);
			}		
		}
	}
};

class Investor : public Observer
{
    std::string name_;

public:
    Investor(const std::string& name) : name_(name)
    {
    }

	void update(const PriceChangedEventArgs& args) override
    {
		std::cout << name_ << " has been notified: " << args.symbol << " - " << args.price << "$" << std::endl;
    }
};

#endif /*STOCK_HPP_*/
