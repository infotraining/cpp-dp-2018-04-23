#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <memory>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

//enum StatisticsType
//{
//    avg, min_max, sum
//};

namespace DataMining
{
	namespace Classics
	{
		class Statistics
		{
		public:
			virtual Results calculate(const Data& data) = 0;
			virtual ~Statistics() = default;
		};

		class Avg : public Statistics
		{
			// Statistics interface
		public:
			Results calculate(const Data& data) override
			{
				double sum = std::accumulate(data.begin(), data.end(), 0.0);
				double avg = sum / data.size();

				return{ { "AVG", avg } };
			}
		};

		class MinMax : public Statistics
		{
			// Statistics interface
		public:
			Results calculate(const Data& data) override
			{
				Data::const_iterator min, max;
				std::tie(min, max) = std::minmax_element(data.cbegin(), data.cend());

				return{ { "MIN", *min },{ "MAX", *max } };
			}
		};

		class Sum : public Statistics
		{
			// Statistics interface
		public:
			Results calculate(const Data& data) override
			{
				double sum = std::accumulate(data.begin(), data.end(), 0.0);

				return{ { "SUM", sum } };
			}
		};

		using StatisticsPtr = std::shared_ptr<Statistics>;

		class StatGroup : public Statistics
		{
			std::vector<StatisticsPtr> stats_;

		public:
			Results calculate(const Data& data) override
			{
				Results results;

				for (const auto& stat : stats_)
				{
					auto rslt = stat->calculate(data);

					std::move(rslt.begin(), rslt.end(), std::back_inserter(results));
				}

				return results;
			}

			void add(StatisticsPtr stat)
			{
				stats_.push_back(stat);
			}
		};
	}

	inline namespace Modern
	{
		using Statistics = std::function<Results(const Data& data)>;

		class Avg
		{
		public:
			Results operator()(const Data& data)
			{
				double sum = std::accumulate(data.begin(), data.end(), 0.0);
				double avg = sum / data.size();

				return{ { "AVG", avg } };
			}
		};

		class MinMax
		{
		public:
			Results operator()(const Data& data)
			{
				Data::const_iterator min, max;
				std::tie(min, max) = std::minmax_element(data.cbegin(), data.cend());

				return{ { "MIN", *min },{ "MAX", *max } };
			}
		};

		class Sum
		{
		public:
			Results operator()(const Data& data)
			{
				double sum = std::accumulate(data.begin(), data.end(), 0.0);

				return{ { "SUM", sum } };
			}
		};

		class StatGroup
		{
			std::vector<Statistics> stats_;

		public:
			Results operator()(const Data& data)
			{
				Results results;

				for (const auto& stat : stats_)
				{
					auto rslt = stat(data);
					std::move(rslt.begin(), rslt.end(), std::back_inserter(results));
				}

				return results;
			}

			void add(Statistics stat)
			{
				stats_.push_back(stat);
			}
		};
	}
}

using namespace DataMining;

class DataAnalyzer
{
	Statistics statistics_;
	Data data_;
	Results results_;

public:
	DataAnalyzer(Statistics statistics) : statistics_{ statistics }
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();
		results_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

	void set_statistics(Statistics statistics)
	{
		statistics_ = statistics;
	}

	void calculate()
	{
		auto temp_results = statistics_(data_);
		move(temp_results.begin(), temp_results.end(), back_inserter(results_));
	}

	const Results& results() const
	{
		return results_;
	}
};

void show_results(const Results& results)
{
	for (const auto& rslt : results)
		std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
	Avg avg;
	MinMax min_max;
	Sum sum;

	StatGroup std_stats;
	std_stats.add(avg);
	std_stats.add(min_max);
	std_stats.add(sum);

	DataAnalyzer da{ std_stats };
	da.load_data("data.dat");
	da.calculate();

	show_results(da.results());

	std::cout << "\n\n";

	da.load_data("new_data.dat");
	da.calculate();

	show_results(da.results());
}
