#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <memory>
#include <functional>
#include <sstream>

class BankAccount
{
    class AccountState
    {
    public:
        virtual void withdraw(BankAccount& ac, double amount) const = 0;
        virtual void pay_interest(BankAccount& ac) const = 0;
        virtual std::string status() const = 0;
        virtual ~AccountState() = default;
    };

    class AccountStateBase : public AccountState
    {
        const double interest_rate_;
    public:
        AccountStateBase(double interest_rate);
        void pay_interest(BankAccount& ac) const override;
    };

    class NormalState : public AccountStateBase
    {
    public:
        using AccountStateBase::AccountStateBase;
        void withdraw(BankAccount& ac, double amount) const override;
        std::string status() const override;
    };

    class OverdraftState : public AccountStateBase
    {
    public:
        using AccountStateBase::AccountStateBase;
        void withdraw(BankAccount& ac, double amount) const override;
        std::string status() const override;
    };

	int id_;
	double balance_;

    // states
    static NormalState normal_state_;
    static OverdraftState overdraft_state_;

    const AccountState* state_;
protected:
    void update_account_state()
	{
		if (balance_ < 0)
            state_ = &overdraft_state_;
		else
            state_ = &normal_state_;
	}

	void set_balance(double amount)
	{
		balance_ = amount;
	}
public:
    BankAccount(int id) : id_(id), balance_(0.0), state_(&normal_state_) {}

	void withdraw(double amount)
	{
		assert(amount > 0);

        state_->withdraw(*this, amount);

        update_account_state();
	}

	void deposit(double amount)
	{
		assert(amount > 0);

		balance_ += amount;

		update_account_state();
	}

	void pay_interest()
	{
        state_->pay_interest(*this);
	}

    std::string status() const
	{
        std::stringstream strm;
        strm << "BankAccount #" << id_
             << "; State: " << state_->status()
             << "; Balance: " << balance_;

        return strm.str();
	}

	double balance() const
	{
		return balance_;
	}

	int id() const
	{
		return id_;
	}
};

#endif

BankAccount::AccountStateBase::AccountStateBase(double interest_rate) : interest_rate_{interest_rate}
{
}

void BankAccount::AccountStateBase::pay_interest(BankAccount& ac) const
{
    ac.balance_ += ac.balance_ * interest_rate_;
}

void BankAccount::NormalState::withdraw(BankAccount& ac, double amount) const
{
    ac.balance_ -= amount;
}

std::string BankAccount::NormalState::status() const
{
    return "Normal";
}

void BankAccount::OverdraftState::withdraw(BankAccount& ac, double amount) const
{
    std::cout << "Brak srodkow na koncie #" << ac.id_ <<  std::endl;
}

std::string BankAccount::OverdraftState::status() const
{
    return "Overdraft";
}
