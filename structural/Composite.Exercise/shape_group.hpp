#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <memory>
#include <vector>

#include "shape.hpp"

namespace Drawing
{
	using ShapePtr = std::unique_ptr<Shape>;

    class ShapeGroup : public CloneableShape<ShapeGroup>
    {		
		std::vector<ShapePtr> shapes_;
    public:
		static constexpr auto id = "ShapeGroup";

		ShapeGroup() = default;


	    ShapeGroup(const ShapeGroup& other)
	    {
			for (const auto& s : other.shapes_)
				shapes_.push_back(s->clone());
	    }

	    ShapeGroup(ShapeGroup&& other) noexcept = default;

	    ShapeGroup& operator=(const ShapeGroup& other)
	    {
		    if (this == &other)
			    return *this;

			shapes_.clear();

			for (const auto& s : other.shapes_)
				shapes_.push_back(s->clone());
		    
		    return *this;
	    }

	    ShapeGroup& operator=(ShapeGroup&& other) noexcept = default;

	    void move(int dx, int dy) override
	    {
			for (const auto& s : shapes_)
				s->move(dx, dy);
	    }

	    void draw() const override
	    {
			for (const auto& s : shapes_)
				s->draw();
	    }

		void add(ShapePtr s)
	    {
			shapes_.push_back(std::move(s));
	    }

		size_t size() const
	    {
			return shapes_.size();
	    }

		using iterator = std::vector<ShapePtr>::iterator;
		using const_iterator = std::vector<ShapePtr>::const_iterator;

		iterator begin()
		{
			return shapes_.begin();
		}

		iterator end()
		{
			return shapes_.end();
		}

		const_iterator begin() const
		{
			return shapes_.begin();
		}

		const_iterator end() const
		{
			return shapes_.end();
		}
    };
}

#endif // SHAPEGROUP_HPP
