#include "shape_group_reader_writer.hpp"

using namespace std;
using namespace Drawing;
using namespace Drawing::IO;

namespace
{
	bool is_registered =
		SingletonShapeRWFactory::instance()
		.register_creator(make_type_index<ShapeGroup>(),
			[] { return make_unique<ShapeGroupReaderWriter>(
				SingletonShapeFactory::instance(),
				SingletonShapeRWFactory::instance()); });
}

void ShapeGroupReaderWriter::read(Shape& shp, std::istream& in)
{
	ShapeGroup& sg = static_cast<ShapeGroup&>(shp);

	int size;

	in >> size;

	for(int i = 0; i < size; ++i)
	{
		string shape_id;
		in >> shape_id;

		auto shape = shape_factory_.create(shape_id);
		auto shape_rw = shape_rw_factory_.create(make_type_index(*shape));

		shape_rw->read(*shape, in);

		sg.add(move(shape));
	}
}

void ShapeGroupReaderWriter::write(const Shape& shp, std::ostream& out)
{
	const ShapeGroup& sg = static_cast<const ShapeGroup&>(shp);

	out << ShapeGroup::id << " " << sg.size();

	for(const auto& s : sg)
	{
		auto shape_rw = shape_rw_factory_.create(make_type_index(*s));
		shape_rw->write(*s, out);
	}
}

	
