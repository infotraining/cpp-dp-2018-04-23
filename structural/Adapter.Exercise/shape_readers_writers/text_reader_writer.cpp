#include "text_reader_writer.hpp"
#include "../shape_factories.hpp"
#include "../text.hpp"

using namespace std;	
using namespace Drawing;
using namespace IO;

// TODO - register creator for a TextReaderWriter class
namespace
{
	bool is_registered = SingletonShapeRWFactory::instance()
		.register_creator(make_type_index<ObjectAdapter::Text>(), &make_unique<TextReaderWriter>);
}

void TextReaderWriter::read(Shape& shp, istream& in)
{
	ObjectAdapter::Text& text = static_cast<ObjectAdapter::Text&>(shp);

	Point pt;
	string txt;

	in >> pt >> txt;

	text.set_coord(pt);
	text.set_text(txt);
}

void TextReaderWriter::write(const Shape& shp, ostream& out)
{
	const ObjectAdapter::Text& text = static_cast<const ObjectAdapter::Text&>(shp);

	out << Text::id << " " << text.coord() << " " << text.text() << std::endl;
}
