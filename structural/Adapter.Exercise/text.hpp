#ifndef TEXT_HPP
#define TEXT_HPP

#include "paragraph.hpp"
#include "shape.hpp"
#include <string>
#include <memory>

namespace Drawing
{
	inline namespace ClassAdapter
	{
		class Text : public ShapeBase, private LegacyCode::Paragraph
		{
		public:
			static constexpr auto id = "Text";

			explicit Text(int x = 0, int y = 0, const std::string& text = "")
				: ShapeBase{ x, y }, LegacyCode::Paragraph{ text.c_str() }
			{
			}

			void draw() const override
			{
				render_at(coord().x, coord().y);
			}

			std::string text() const
			{
				return get_paragraph();
			}

			void set_text(const std::string& text)
			{
				set_paragraph(text.c_str());
			}
		};
	}

	namespace ObjectAdapter
	{
		class Text : public ShapeBase
		{
			std::unique_ptr<LegacyCode::Paragraph> par_;
		public:
			static constexpr auto id = "Text";

			explicit Text(std::unique_ptr<LegacyCode::Paragraph> par, int x = 0, int y = 0)
				: ShapeBase{ x, y }, par_{ std::move(par) }
			{
			}

			void draw() const override
			{
				par_->render_at(coord().x, coord().y);
			}

			std::string text() const
			{
				return par_->get_paragraph();
			}

			void set_text(const std::string& text)
			{
				par_->set_paragraph(text.c_str());
			}
		};
	}
}

#endif
